@extends('layout.master')

@section('content')
    <h1>Create product</h1>
    <form action="/products/{{ $product->id }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
        <input type="text" name="name" value="{{ $product->name }}" required>
        <br>
        <input type="text" name="price" value="{{ $product->price }}" required>
        <br>
        <input type="submit" value="Update">        
    </form>
@stop