@extends('layout.master')

@section('content')
    <h1>Products</h1>
    <a href="/products/create">New</a>
    <table>
        <tr>
            <th>Name</th>
            <th>Price</th>
        </tr>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>
                <a href="/products/{{ $product->id }}">Show</a>
                <a href="/products/{{ $product->id }}/edit">Edit</a>
                <form action="/products/{{ $product->id }}" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" value="Delete">
                </form>
            </td>
        </tr>
    @endforeach
    </table>
@stop