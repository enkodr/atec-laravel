@extends('layout.master')

@section('content')
    <h1>Create product</h1>
    @if(sizeof($errors) > 0 )   
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif
    <form action="/products" method="post">
        {{ csrf_field() }}
        <input type="text" name="name">
        <br>
        <input type="text" name="price">
        <br>
        <input type="submit" value="Create">        
    </form>
@stop