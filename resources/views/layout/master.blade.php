<html>
    <head>
        <title>Awesome Page</title>
        <style> 
            nav {
                line-height: 50px;
                text-align: center;
                border: 1px solid #000;
            }
            .leftbar {
                float: left;
                width: 200px;
                height: 400px;
            }
            .content {

            }
        </style>
    </head>
    <body>
        <nav>
            <a href="/">Home</a>
            <a href="/about">About</a>
            <a href="/products">Products</a>
        </nav>
        <div class="leftbar">
            Noticias
        </div>
        <div class="content">
            Conteudo
            @yield('content')
        </div>
    </body>
</html>